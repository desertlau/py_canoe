# [py_canoe](https://github.com/chaitu-ycr/py_canoe)

Python 🐍 Package for controlling Vector CANoe 🛶 Tool

I want to thank plants 🎋 for providing me oxygen each day.
Also, I want to thank the sun 🌄 for providing more than half of their nourishment free of charge.
