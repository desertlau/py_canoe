# python environment setup

create python virtual environment

```bat
python -m venv .venv
```

activate virtual environment

```bat
.venv\Scripts\activate
```

upgrade pip

```bat
python -m pip install pip --upgrade
```

install [py_canoe](https://pypi.org/project/py_canoe/) module

```bat
pip install py_canoe --upgrade
```
